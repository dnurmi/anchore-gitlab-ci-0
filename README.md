# anchore-gitlab-ci-0

The top-level of this repo includes a Dockerfile and .gitlab-ci.yml
that will trigger a ci/cd build in gitlab to build a container, push
it to the gitlab registry, and then use an anchore container to
perform a scan of the container and generate output that can be
uploaded to gitlab to show results in gitlabs's sast security
dashboard.

the citool/ directory contains all the code and Dockerfiles used to
generate the anchore container referenced in the .gitlab-ci.yml.  The
only code not included in regular anchore tools is a prototype (but
fully functional) program called 'optymus' which transforms output of
an anchore vulnerability scan json (from API) into gitlab's SAST
format.  The top level script in citool called 'anchore-gitlab-scan'
facilitates the flow of adding, waiting, fetching, and transforming
the data.

