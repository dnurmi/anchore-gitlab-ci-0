#!/bin/bash

for i in anchore-gitlab-scan-dind anchore-gitlab-scan-api
do
    docker build -t docker.io/dnurmi/testrepo:${i} -f Dockerfile.${i} .
    docker push docker.io/dnurmi/testrepo:${i}
done
