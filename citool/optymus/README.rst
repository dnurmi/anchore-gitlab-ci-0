Ex: convert anchore API JSON to gitlab SAST report format

anchore-cli --json image get ${ANCHORE_IMAGE} > /tmp/anchore-image.json
anchore-cli --json image vuln ${ANCHORE_IMAGE} all > /tmp/anchore-vulnerabilities.json
optymus transform cicd --input-format anchore-api --output-format gitlab-sast /tmp/anchore-vulnerabilities.json /tmp/anchore-image.json > gitlab-sast-ready.json

Ex: convert anchore grype JSON to gitlab SAST report format

grype -o json ${ANCHORE_IMAGE} > /tmp/anchore-vulnerabilities.json
optymus transform cicd --input-format anchore-grype --output-format gitlab-sast /tmp/anchore-vulnerabilities.json > gitlab-sast-ready.json
