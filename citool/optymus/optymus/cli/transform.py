import os
import sys
import json
import time
import click
import logging
import hashlib

_logger = logging.getLogger(__name__)

gitlab_sev_map = {
    "critical": "Critical",
    "high": "High",
    "medium": "Medium",
    "low": "Low",
    "negligible": "Info",
    "unknown": "Info",
}

gitlab_sast_template = {
    "version": "2.3",
    "vulnerabilities": [],
    "remediations": []
}

def _transform_api_to_gitlab_sast(vulns, image_data):
    ret = []
    for vuln in vulns.get('vulnerabilities', []):
        vid = vuln.get('vuln')
        vsev = vuln.get('severity', 'unknown')
        gvsev = gitlab_sev_map.get(vsev.lower(), "Info")
        vlinks = [vuln.get('url', "Unavailable")]

        image = image_data.get('image_detail', [{}])[0].get('fulltag', "")
        distro = image_data.get('image_content', {}).get('metadata', {}).get('distro', None)
        distro_vers = image_data.get('image_content', {}).get('metadata', {}).get('distro_version', None)
        if distro and distro_vers:
            matcher = "{}:{}".format(distro, distro_vers)
        else:
            matcher = vuln.get('feed_group')

        pkgname = vuln.get('package_name')
        pkgvers = vuln.get('package_version')
        pkglocs = [vuln.get('package_path')]
        if pkglocs:
            pkgloc = pkglocs[0]
        else:
            pkgloc = "NA"

        solution = 'Unknown'
        pkgfix = vuln.get('fix', 'None')
        if pkgfix != 'None':
            solution = "There is a security fix for package '{}' - upgrade to version '{}'".format(pkgname, pkgfix)

        confidence = 'Confirmed'
        description = "There is a vulnerability {} detected in installed package {}.  See link for more information.".format(vid, pkg)
        
        pkg = vuln.get('package')

        scanner = "anchore-enterprise"
        vhash = hashlib.sha256(bytes("{}-{}-{}".format(vid, pkg, pkgloc), "UTF-8")).hexdigest()

        if vlinks:
            vlink = vlinks[0]
        else:
            vlink = ""

        gvuln = {
            #"id": vhash,
            "cve": "{}:{}:{}".format(image, pkg, vid),
            "category": "container_scanning",
            "message": "{} in {}".format(vid, pkg),
            "description": description,
            "severity": gvsev,
            "confidence": confidence,
            "solution": solution,
            "scanner": {
                "id": scanner,
                "name": scanner
            },
            "location": {
                "dependency": {
                    "package": {
                        "name": "{}".format(pkgname)
                    },
                    "version": pkgvers
                },
                "operating_system": matcher,
                "image": image
            },
            "identifiers": [
                {
                    "type": "cve",
                    "name": vid,
                    "value": vid,
                    "url": vlink
                }
            ],
            "links": []
        }
        for vl in vlinks:
            gvuln['links'].append(
                {
                    "url": vl
                }
            )
        ret.append(gvuln)
        
    return(ret)

def _transform_grype_to_gitlab_sast(vulns):
    ret = []

    image = vulns.get('image', {}).get('tags', [""])[0]
    if not image:
        image = vulns.get('image', {}).get('digest', "unknown")
        
    matches = vulns.get('matches', [])
    for vuln in matches:
        vid = vuln.get('vulnerability', {}).get('id')
        vsev = vuln.get('vulnerability', {}).get('severity', "unknown")
        gvsev = gitlab_sev_map.get(vsev.lower(), "Info")
        desc = vuln.get('vulnerability', {}).get('description', "Unavailable")
        vlinks = vuln.get('vulnerability', {}).get('links', [])
        matcher = vuln.get('matched-by', {}).get('matcher', "unknown")

        pkgname = vuln.get('artifact', {}).get('name')
        pkgvers = vuln.get('artifact', {}).get('version')
        pkglocs = vuln.get('artifact', {}).get('locations', [])
        if pkglocs:
            pkgloc = pkglocs[0].get('path', "NA")
        else:
            pkgloc = "NA"    
        pkg = "{}-{}".format(pkgname, pkgvers)


        scanner = "anchore-grype"
        vhash = hashlib.sha256(bytes("{}-{}-{}".format(vid, pkg, pkgloc), "UTF-8")).hexdigest()

        if vlinks:
            vlink = vlinks[0]
        else:
            vlink = ""

        gvuln = {
    #        "id": vhash,
            "cve": "{}:{}:{}".format(image, pkg, vid),
            "category": "container_scanning",
            "message": "{} in {}".format(vid, pkg),
            "description": desc,
            "severity": gvsev,
            "confidence": "Unknown",
            "solution": "Unknown",
            "scanner": {
                "id": scanner,
                "name": scanner
            },
            "location": {
                "dependency": {
                    "package": {
                        "name": "{}".format(pkgname)
                    },
                    "version": pkgvers
                },
                "operating_system": matcher,
                "image": image
            },
            "identifiers": [
                {
                    "type": "cve",
                    "name": vid,
                    "value": vid,
                    "url": vlink
                }
            ],
            "links": []
        }
        for vl in vlinks:
            gvuln['links'].append(
                {
                    "url": vl
                }
            )
        ret.append(gvuln)
    
    return(ret)

@click.group(name='transform', short_help='Transform between anchore and platform formats')
@click.pass_context
@click.pass_obj
def transform(ctx_config, ctx):
    global config
    config = ctx_config

@transform.command(name='cicd', short_help="Anchore format to CICD security formats")
@click.option('--input-format', help="Input format", required=True)
@click.option('--output-format', help="Output format", required=True)
#@click.option('--image', help="Container image full tag string", required=False)
@click.argument('input_files', nargs=-1)
def cicd(input_format, output_format, input_files):
    output_data = {}
    
    if input_format == 'anchore-grype':
        with open(input_files[0], 'r') as FH:
            vulns = json.loads(FH.read())
        #with open(input_files[1], 'r') as FH:            
        #    image_meta = json.loads(FH.read())
        if output_format == 'gitlab-sast':
            output_data = _transform_grype_to_gitlab_sast(vulns)
            
    elif input_format == 'anchore-api':
        with open(input_files[0], 'r') as FH:
            vulns = json.loads(FH.read())
        with open(input_files[1], 'r') as FH:            
            image_meta = json.loads(FH.read())[0]        
        if output_format == 'gitlab-sast':
            output_data = _transform_api_to_gitlab_sast(vulns, image_meta)            

    else:
        raise Exception("input-format {} unknown".format(input_format))

    output = {}
    output.update(gitlab_sast_template)
    output['vulnerabilities'] = output_data
    print ("{}".format(json.dumps(output, indent=4)))
