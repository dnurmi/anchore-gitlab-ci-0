import click
        
from . import transform

from optymus import version

@click.group(context_settings=dict(help_option_names=['-h', '--help', 'help']))
@click.option('--debug', is_flag=True, help='Debug output to stderr')
#@click.option('--input-format', help='Input format type')
#@click.option('--output-format', help='Output format type')
@click.version_option(version=version.version)
@click.pass_context
def main_entry(ctx, debug):
    config = {
        'debug': debug,
    }
    ctx.obj = config

class Help(click.Command):
    """
    Do not parse any arguments, allow any args past the `help` command,
    always return the help output
    """

    def parse_args(self, ctx, args):
        return []


@click.command(cls=Help)
@click.pass_context
def help(ctx):
    print(ctx.parent.get_help())


main_entry.add_command(help)
main_entry.add_command(transform.transform)
